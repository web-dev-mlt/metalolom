var openmodal = document.querySelectorAll('.modal-open');
if (openmodal !== null){
  for (var i = 0; i < openmodal.length; i++) {
    openmodal[i].addEventListener('click', function(event){
      event.preventDefault();
      toggleModal();

      const modal = document.querySelector('.modal');
      modal.querySelector('form input[name=form_name]').value = event.target.textContent;
      modal.querySelector('.form-title-js').textContent = event.target.textContent;
    })
  }
}

const overlay = document.querySelector('.modal-overlay');
if (overlay !== null){
  overlay.addEventListener('click', toggleModal);
}

var closemodal = document.querySelectorAll('.modal-close');
if (closemodal !== null){
  for (var i = 0; i < closemodal.length; i++) {
    closemodal[i].addEventListener('click', toggleModal);
  }
}

document.onkeydown = function(evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc")
    } else {
        isEscape = (evt.keyCode === 27);
    }
    if (isEscape && document.body.classList.contains('modal-active')) {
        toggleModal();
    }
};


function toggleModal () {
    const body = document.querySelector('body');
    const modal = document.querySelector('.modal');
    modal.classList.toggle('opacity-0');
    modal.classList.toggle('pointer-events-none');
    body.classList.toggle('modal-active');
}
