$.event.special.touchstart = {
  setup: function( _, ns, handle ){
    if ( ns.includes("noPreventDefault") ) {
      this.addEventListener("touchstart", handle, { passive: false });
    } else {
      this.addEventListener("touchstart", handle, { passive: true });
    }
  }
};

$.event.special.touchmove = {
  setup: function( _, ns, handle ){
    if ( ns.includes("noPreventDefault") ) {
      this.addEventListener("touchmove", handle, { passive: false });
    } else {
      this.addEventListener("touchmove", handle, { passive: true });
    }
  }
};

var nav = document.getElementById('site-menu');
var header = document.getElementById('top');

if(nav !== null && header !== null){
  window.addEventListener('scroll', function() {
    if (window.scrollY >=400) { // adjust this value based on site structure and header image height
      nav.classList.add('nav-sticky');
      header.classList.add('pt-scroll');
    } else {
      nav.classList.remove('nav-sticky');
      header.classList.remove('pt-scroll');
    }
  });
}

function navToggle() {
    var btn = document.getElementById('menuBtn');
    var nav = document.getElementById('menu');

    btn.classList.toggle('open');
    nav.classList.toggle('flex');
    nav.classList.toggle('hidden');
}
//INPUT FILE
window.onload = function() {
  if ($(window).width() > 768) {
    let h = $('.wizard > .steps').height();
    $('.wizard > .content').css('height',h);
  }
  else{
    $('.wizard > .content').css('height','auto');
  }


  $( window ).resize(function() {
    if ($(window).width() > 768) {
      let h = $('.wizard > .steps').height();
      $('.wizard > .content').css('height',h);
    }
    else{
      $('.wizard > .content').css('height','auto');
    }
  });
  $('#file-upload').change(function() {
    var i = $(this).prev('label').clone();
    var file = $('#file-upload')[0].files[0].name;
    $(this).prev('label').text(file);
  });
};
//INPUT NUMBER
function decrement(e) {
  const btn = e.target.parentNode.parentElement.parentElement.querySelector(
      'button[data-action="decrement"]'
  );
  let price = $(e.target).parentsUntil('tr').parent().find('td.price-in-table');
  const target = btn.nextElementSibling;
  let value = Number(target.value);
  value--;
  if(value>=1){
    price.text('от '+price.attr('data-price')*value+' руб');
    target.value = value;
  }

}

function increment(e) {
  const btn = e.target.parentNode.parentElement.parentElement.querySelector(
      'button[data-action="decrement"]'
  );
  let price = $(e.target).parentsUntil('tr').parent().find('td.price-in-table');
  const target = btn.nextElementSibling;
  let value = Number(target.value);
  value++;
  price.text('от '+price.attr('data-price')*value+' руб');
  target.value = value;
}

const decrementButtons = document.querySelectorAll(
    `button[data-action="decrement"]`
);

const incrementButtons = document.querySelectorAll(
    `button[data-action="increment"]`
);

decrementButtons.forEach(btn => {
  btn.addEventListener("click", decrement);
});


incrementButtons.forEach(btn => {
  btn.addEventListener("click", increment);
});

$('section.table-services-js button.table-btn-more-js').click(function () {
  var btn = $(this);

  btn.prop('disabled', true);

  var table = $('section.table-services-js table');
  var exclude = [];
  var post_type = '';
  var per_page = 0;
  table.find('tbody tr').each(function () {
    exclude.push($(this).attr('data-post-id'));
    post_type = $(this).attr('data-type-post');
    per_page = table.find('tbody').attr('data-per-page');
  });

  var data = {
    exclude: exclude,
    post_type: post_type,
    per_page: per_page,
    action: 'get_posts_and_acf',
  };

  $.post(wp.url, data, function (response) {
    btn.prop('disabled', false);
    table.find('tbody').attr('data-current-count', (parseInt(table.find('tbody').attr('data-current-count')) + parseInt(per_page)));
    if (table.find('tbody').attr('data-current-count') === table.find('tbody').attr('data-all-count')) {
      btn.remove();
    }

    var result = JSON.parse(response);

    show_table_service_item(result, table);
  });

});

function show_table_service_item(items, table) {
  $(items).each(function () {
    $(table).find('tbody').append(
      '<tr data-post-id="' + this.ID + '" data-type-post="' + this.post_type + '">' +
      '<td class="p-4">' +
      '<img src="' + this.acf.photo.sizes.thumbnail + '" alt="' + this.acf.photo.name + '" class="block mx-auto" width="' + this.acf.photo.sizes['thumbnail-width'] + '" height="' + this.acf.photo.sizes['thumbnail-height'] + '"> ' +
      '</td>' +
      '<td>' +
      this.acf.requirement
      + '</td>' +
      '<td class="font-light text-center">' +
      parseInt(this.acf.zasor).toFixed(2) + '%' +
      '</td>' +
      '<td>' +
      '<div class="flex flex-row justify-between p-2 w-full rounded-lg relative bg-transparent mt-1 border-2 border-mgray">' +
      '<button data-action="decrement" class="h-8 w-8 rounded-full text-white hover:text-mlgreen bg-mblue h-8 w-8 cursor-pointer outline-none">' +
      '<span class="m-auto text-2xl font-thin">−</span>' +
      '</button>' +
      '<input type="number" class="outline-none focus:outline-none text-center w-14 font-semibold text-md hover:text-black focus:text-black md:text-basecursor-default flex items-center text-gray-700 outline-none" name="custom-input-number" value="1">' +
      '<button data-action="increment" class="h-8 w-8 rounded-full text-white hover:text-mlgreen bg-mblue h-8 w-8 cursor-pointer">' +
      '<span class="m-auto text-2xl font-thin">+</span>' +
      '</button>' +
      '</div>' +
      '</td>' +
      '<td class="font-bold text-center text-xl price-in-table" data-price="' + this.acf.price + '">' +
      'от ' + this.acf.price + ' руб' +
      '</td>' +
      '</tr>');
  });

  var decrementButtons = document.querySelectorAll(
    `button[data-action="decrement"]`
  );
  var incrementButtons = document.querySelectorAll(
    `button[data-action="increment"]`
  );

  decrementButtons.forEach(btn => {
    btn.addEventListener("click", decrement);
  });

  incrementButtons.forEach(btn => {
    btn.addEventListener("click", increment);
  });
}
