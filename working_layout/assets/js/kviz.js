$(document).ready(function () {
  var form = $("#questions");
  $("#questions").steps({
    headerTag: "span",
    bodyTag: "section",
    actionContainerTag: "div",
    transitionEffect: "slideLeft",
    autoFocus: true,
    saveState: true,
    labels: {
      finish: "Отправить",
      next: "Следующий шаг",
      previous: "Предыдущий шаг"
    },
    stepsOrientation: "vertical",
    onStepChanging: function (event, currentIndex, newIndex) {
      // Always allow going backward even if the current step contains invalid fields!
      if (currentIndex > newIndex) {
        return true;
      }

      // Forbid suppressing "Warning" step if the user is to young
      if (currentIndex === 3 && Number($("input[name='delivery']:checked").length) > 0) {
        return true;
      }


      // Clean up if user went backward before
      if (currentIndex < newIndex) {
        // To remove error styles
        $(".body:eq(" + newIndex + ") label.error", form).remove();
        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
      }

      // Disable validation on fields that are disabled or hidden.
      form.validate().settings.ignore = ":disabled,:hidden";

      // Start validation; Prevent going forward if false
      return form.valid();
    },
    onStepChanged: function (event, currentIndex, newIndex) {
      if (currentIndex > 0) {
        $('.actions > ul > li:first-child').attr('style', '');
      } else {
        $('.actions > ul > li:first-child').attr('style', 'display:none');
      }
      $(".top-actions").empty();
      $("section.body > .actions").css("display", "block");
      $(".actions").css("display", "none");
      $(".actions").clone().appendTo(".top-actions");
    },
    onInit: function (event, currentIndex) {
      $("form#questions input[name=phone]").mask("+7 (999) 999-99-99");
      if (currentIndex > 0) {
        $('.actions > ul > li:first-child').attr('style', '');
      } else {
        $('.actions > ul > li:first-child').attr('style', 'display:none');
      }
      $(".top-actions").empty();
      $("section.body > .actions").css("display", "block");
      $(".actions").css("display", "none");
      $(".actions").clone().appendTo(".top-actions");
    },
    onFinishing: function (event, currentIndex) {
      form.validate().settings.ignore = ":disabled,:hidden";
      return form.valid();
    },
    onFinished: function (event, currentIndex) {

      var $form = $('form#questions');

      var formData = {
        data: $form.serialize(),
        action: 'fluentform_submit',
        form_id: 8
      };

      $.post(wp.url, formData)
        .done(function(res) {
          if(res.data.result.action === "hide_form"){
            $form.empty().append("<h2 class='mb-4 text-black text-center'>Спасибо!</h2>" + "<h2 class='mb-4 text-center'>Ваша заявка принята</h2>" + "<p class='mb-4 text-center'>Наши менеджеры свяжутся с вами в ближайшее время!</p>");
          }
        })
        .fail(function (res) {
          console.log(res);
        });


    }

  })
  // $(".top-actions a[href='#previous']").on('click', function (){
  //     $(".actions:last a[href='#previous']").trigger("click");
  // });
  // $(".top-actions a[href='#next']").on('click', function (){
  //     console.log($(".actions:last a[href='#previous']").text());
  //     $(".actions:last a[href='#next']").trigger("click");
  // })

  $(document).on('click', '.top-actions a[href=\'#previous\']', function () {
    console.log($('.wizard div:last'));
    $('.wizard div:last').find("a[href='#previous']").trigger("click");
  });
  $(document).on('click', '.top-actions a[href=\'#next\']', function () {
    $('.wizard div:last').find("a[href='#next']").trigger("click");
  });
  $(document).on('click', '.top-actions a[href=\'#finish\']', function () {
    $('.wizard div:last').find("a[href='#finish']").trigger("click");
  });

});
