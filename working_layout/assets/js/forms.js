$('#form-1').submit(function (event) {
  event.preventDefault();
  $("#form-1").parent().empty().append("<h2 class='mb-4 text-center'>Спасибо!</h2>" + "<h2 class='mb-4 text-center'>Ваша заявка принята</h2>" + "<p class='mb-4 text-center'>Наши менеджеры свяжутся с вами в ближайшее время!</p>");

});
$('#form-2').submit(function (event) {
  event.preventDefault();
  $("#form-2").parent().empty().append("<h2 class='mb-4 text-center'>Спасибо!</h2>" + "<h2 class='mb-4 text-center'>Ваша заявка принята</h2>" + "<p class='mb-4 text-center'>Наши менеджеры свяжутся с вами в ближайшее время!</p>");

});
$('#form-3').submit(function (event) {
  event.preventDefault();
  $("#form-3").parent().empty().append("<h2 class='mb-4 text-center'>Спасибо!</h2>" + "<h2 class='mb-4 text-center'>Ваша заявка принята</h2>" + "<p class='mb-4 text-center'>Наши менеджеры свяжутся с вами в ближайшее время!</p>");

});
$('#form-4').submit(function (event) {
  event.preventDefault();
  $("#form-4").empty().append("<h2 class='mb-4 text-white text-center'>Спасибо!</h2>" + "<h2 class='mb-4 text-white text-center'>Ваша заявка принята</h2>" + "<p class='mb-4 text-white text-center'>Наши менеджеры свяжутся с вами в ближайшее время!</p>");

});
$('#form-by-photo').submit(function (event) {
  event.preventDefault();
  $("#form-by-photo").parent().empty().append("<h2 class='mb-4 text-center'>Спасибо!</h2>" + "<h2 class='mb-4 text-center'>Ваша заявка принята</h2>" + "<p class='mb-4 text-center'>Наши менеджеры свяжутся с вами в ближайшее время!</p>");

});
$('#popup').submit(function (event) {
  event.preventDefault();
  $("#popup").parent().empty().append("<h2 class='mb-4 text-center'>Спасибо!</h2>" + "<h2 class='mb-4 text-center'>Ваша заявка принята</h2>" + "<p class='mb-4 text-center'>Наши менеджеры свяжутся с вами в ближайшее время!</p>");

});

$("#form-record").submit(function (e) {
  e.preventDefault();
  var form = $(this);
  var formData = {
    data: $(this).serialize(),
    action: 'fluentform_submit',
    form_id: 5
  };
  $.post(wp.url, formData)
    .done(function (res) {
      if (res.data.result.action === "hide_form") {
        $('<div/>', {
          'id': formData.form_id + '_success',
          'class': 'ff-message-success text-white h-full',
          'style': 'margin: 0;padding: 1rem;'
        })
          .html(res.data.result.message)
          .insertAfter(form);
        form.hide();
      }
    })
    .fail(function (res) {
      console.log(res);
    });
});
