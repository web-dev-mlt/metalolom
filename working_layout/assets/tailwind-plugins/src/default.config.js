/**
 * Default gutenberg components config
 */
module.exports = theme => ({
  /**
   * Wrapping element
   */
  wrapper: {
    'selector': '.entry-content',
  },

  /**
   * Support certain feature subsets
   */
  supports: {
    wideAlignments: true,
    userTypeScale: true,
    fixedLayoutTable: true,
  },

  /**
   * Support block styles
   */
  styles: {
    'stripes': true,
  },

  /**
   * Screensizes for @media queries
   */
  screens: {
    'sm': theme('screens.sm'),
    'md': theme('screens.md'),
    'lg': theme('screens.lg'),
    'xl': theme('screens.xl'),
  },

  /**
   * Block contents
   */
  blockContent: {
    maxWidth: {
      xs: {
        normal: theme('maxWidth.xl'),
        wide:   theme('maxWidth.full'),
        full:   theme('maxWidth.full'),
      },
      sm: {
        normal: theme('maxWidth.2xl'),
        wide:   theme('maxWidth.full'),
        full:   theme('maxWidth.full'),
      },
      md: {
        normal: theme('maxWidth.3xl'),
        wide:   theme('maxWidth.4xl'),
        full:   theme('maxWidth.full'),
      },
      lg: {
        normal: theme('maxWidth.4xl'),
        wide:   theme('maxWidth.5xl'),
        full:   theme('maxWidth.full'),
      },
      xl: {
        normal: theme('maxWidth.5xl'),
        wide:   theme('maxWidth.6xl'),
        full:   theme('maxWidth.full'),
      },
    },
  },

  /**
   * Spacing units
   */
  spacing: {
    horizontal: theme('spacing.4'),
    vertical: {
      default: theme('spacing.8'),
      wide: theme('spacing.12'),
      full: theme('spacing.16'),
    },
  },

  /**
   * Typography
   */
  typography: {
    fontFamily: {
      'h1': theme('fontFamily.regular'),
      'h2': theme('fontFamily.regular'),
      'h3': theme('fontFamily.regular'),
      'h4': theme('fontFamily.regular'),
      'h5': theme('fontFamily.regular'),
      'h6': theme('fontFamily.regular'),
      'p':  theme('fontFamily.regular'),
      'ul': theme('fontFamily.regular'),
      'ol': theme('fontFamily.regular'),
      'figcaption': theme('fontFamily.regular'),
      'table': theme('fontFamily.regular'),
      'code': theme('fontFamily.regular'),
      'pre': theme('fontFamily.regular'),
    },

  },

  /**
   * Keys for `gutenberg.figCaption.textAlign`
   * map to the alignment of the containing block
   */
  figCaption: {
    textAlign: {
      left:   'left',
      right:  'right',
      center: 'center',
      wide:   'center',
    },
  },

  /**
   * Lists: unordered & ordered
   */
  lists: {
    inset: theme('spacing.2'),
    orderedStyle: 'none',
    unorderedStyle: 'none',
  },

  /**
   * Block specific configuration values
   */
  blocks: {
    /**
     * core/cover
     */
    cover: {
      contentColor: theme('colors.white'),
      verticalPadding: theme('spacing.64'),
      itemsSpacing: theme('spacing.4'),
    },

    /**
     * core/table
     */
    table: {
      head: {
        fontWeight: theme('fontWeight.semibold'),
      },
      cell: {
        borderColor: theme('colors.gray.200'),
        borderWidth: theme('borderWidth.default'),
        padding: theme('spacing.2'),
      },
      stripes: {
        color: theme('colors.gray.100'),
      },
    },
  },
})
