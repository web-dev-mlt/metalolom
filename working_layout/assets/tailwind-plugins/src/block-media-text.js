/**
 * Block Content
 */
module.exports = ({ addComponents, theme }) => {
  const { mediaText, screens, spacing } = theme('gutenberg')

  const block = {
    '.wp-block-media-text': {
      gridAutoFlow: 'dense',
      gridColumnGap: spacing.horizontal,

      [`@media (max-width: ${screens.xs})`]: {
        display: 'inherit',

        '&.has-media-on-the-right &__media': {
          marginBottom: spacing.vertical.default
        },
      },

      [`@media (min-width: ${screens.lg})`]: {
        display: 'grid',

        '&.has-media-on-the-right &__media': {
          marginBottom: 0
        },
      },

      '&.has-media-on-the-right &__media': {
         gridColumn: '2'
      },
      '&.has-media-on-the-right &__content': {
        gridColumn: '1'
      }



    },
  }

  addComponents(block)
}
