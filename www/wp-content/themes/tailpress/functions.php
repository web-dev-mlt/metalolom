<?php
include('inc/include.php');
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

add_filter( 'script_loader_src', '_remove_script_version' );
add_filter( 'style_loader_src', '_remove_script_version' );
function _remove_script_version( $src ){
    $parts = explode( '?', $src );
    return $parts[0];
}

// Use a quality setting of 75 for WebP images.
function filter_webp_quality( $quality, $mime_type ) {
    if ( 'image/webp' === $mime_type ) {
        return 75;
    }
    return $quality;
}
add_filter( 'wp_editor_set_quality', 'filter_webp_quality', 10, 2 );

// Ensure all network sites include WebP support.
add_filter(
    'site_option_upload_filetypes',
    function ( $filetypes ) {
        $filetypes = explode( ' ', $filetypes );
        if ( ! in_array( 'webp', $filetypes, true ) ) {
            $filetypes[] = 'webp';
            $filetypes   = implode( ' ', $filetypes );
        }

        return $filetypes;
    }
);

//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes) {
    $existing_mimes['webp'] = 'image/webp';
    return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');

function webp_is_displayable($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );

        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }

    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);

add_filter( 'wp_enqueue_scripts', 'change_default_jquery', PHP_INT_MAX );

function change_default_jquery( ){
    if ( !is_admin() ) {
        wp_dequeue_script( 'jquery');
        wp_deregister_script( 'jquery');
    }
}


/**
 * Enqueue scripts.
 */
function tailpress_enqueue_scripts() {
	wp_enqueue_style( 'tailpress', tailpress_get_mix_compiled_asset_url( 'css/all.css' ), array(), false );
	wp_enqueue_script( 'jquery', tailpress_get_mix_compiled_asset_url( 'js/app.js' ), array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'tailpress_enqueue_scripts', PHP_INT_MAX );

add_action('wp_head', 'ajaxurl');
function ajaxurl() {

    echo '<script>
           var wp = {
               url : "' . admin_url('admin-ajax.php') . '"
           };
           var fluentFormVars = {
              "ajaxUrl": "' . admin_url('admin-ajax.php') . '",
              "forms": [],
              "step_text": "Step %activeStep% of %totalStep% - %stepTitle%",
              "is_rtl": "",
              "date_i18n": {
                "previousMonth": "Previous Month",
                "nextMonth": "Next Month",
                "months": {
                  "shorthand": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                  "longhand": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                },
                "weekdays": {
                  "longhand": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                  "shorthand": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
                },
                "daysInMonth": [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
                "rangeSeparator": " to ",
                "weekAbbreviation": "Wk",
                "scrollTitle": "Scroll to increment",
                "toggleTitle": "Click to toggle",
                "amPM": ["AM", "PM"],
                "yearAriaLabel": "Year"
              },
              "pro_version": "4.1.5",
              "fluentform_version": "3.6.62",
              "force_init": "",
              "stepAnimationDuration": "350",
              "upload_completed_txt": "100% Completed",
              "upload_start_txt": "0% Completed",
              "uploading_txt": "Uploading",
              "choice_js_vars": {
                "noResultsText": "No results found",
                "loadingText": "Loading...",
                "noChoicesText": "No choices to choose from",
                "itemSelectText": "Press to select"
              }
            };
           
         </script>';
}

add_filter( 'inline_javascript_in_footer_handles', 'my_inline_javascript_in_footer_handles', 99999 );
function my_inline_javascript_in_footer_handles( $handles ) {
    $scripts = [
        'smush-lazy-load', 'intlTelInput', 'intlTelInputUtils', 'fluent-form-submission',
        'jquery-mask', 'fluentform-uploader-jquery-ui-widget', 'fluentform-uploader-iframe-transport',
        'fluentform-uploader', 'fluentform-advanced', 'currency'
    ];
    return array_merge( $handles, $scripts );
}

add_filter( 'inline_css_in_head_handles', 'my_inline_css_in_head_handles', 99999 );
function my_inline_css_in_head_handles( $handles ) {
    $styles = [
        'wp-block-editor', 'embedpress_blocks-cgb-style-css', 'embedpress_pro-cgb-style-css', 'embedpress',
        'intlTelInput', 'fluent-form-styles', 'fluentform-public-default', 'wp-reusable-blocks',
        'wp-nux', 'wp-editor', 'wp-components'
    ];
    return array_merge( $handles, $styles );
}

/**
 * Get mix compiled asset.
 *
 * @param string $path The path to the asset.
 *
 * @return string
 */
function tailpress_get_mix_compiled_asset_url( $path ) {
	$path                = '/' . $path;
	$stylesheet_dir_uri  = get_stylesheet_directory_uri();
	$stylesheet_dir_path = get_stylesheet_directory();

	if ( ! file_exists( $stylesheet_dir_path . '/mix-manifest.json' ) ) {
		return $stylesheet_dir_uri . $path;
	}

	$mix_file_path = file_get_contents( $stylesheet_dir_path . '/mix-manifest.json' );
	$manifest      = json_decode( $mix_file_path, true );
	$asset_path    = ! empty( $manifest[ $path ] ) ? $manifest[ $path ] : $path;

	return $stylesheet_dir_uri . $asset_path;
}

function acf_wysiwyg_remove_wpautop() {
    remove_filter('acf_the_content', 'wpautop' );
}

/**
 * Theme setup.
 */
function tailpress_setup() {

    remove_filter ('the_content', 'wpautop');
    remove_filter ('the_excerpt', 'wpautop');
    add_action('acf/init', 'acf_wysiwyg_remove_wpautop');
    add_filter( 'term_description', 'wpautop' );
    add_filter( 'get_the_post_type_description', 'wpautop' );
    add_filter( 'comment_text', 'wpautop', 30 );
    add_filter( 'widget_text_content', 'wpautop' );
    add_filter( 'the_excerpt_embed', 'wpautop' );


    add_theme_support( 'title-tag' );
	add_theme_support(
		'html5',
		array(
			'gallery',
			'caption',
		)
	);
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'align-wide' );
	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'editor-styles' );
	add_editor_style();

    acf_add_options_page(array(
        'page_title' => 'Баннер на главной',
        'menu_slug' => 'banner',
        'menu_title' => 'Баннер на главной',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'banner',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок формы на главной',
        'menu_slug' => 'form_with_contact',
        'menu_title' => 'Блок формы на главной',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'form_with_contact',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок "Схема работы"',
        'menu_slug' => 'scheme_of_work',
        'menu_title' => 'Блок "Схема работы"',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'scheme_of_work',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок "Преимуществ"',
        'menu_slug' => 'privilege',
        'menu_title' => 'Блок "Преимуществ"',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'privilege',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок "Оценим по фото"',
        'menu_slug' => 'form_assessment',
        'menu_title' => 'Блок "Оценим по фото"',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'form_assessment',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок "Гарантии при приеме"',
        'menu_slug' => 'guarantee',
        'menu_title' => 'Блок "Гарантии при приеме"',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'guarantee',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок "Удобства работы"',
        'menu_slug' => 'convenience-work',
        'menu_title' => 'Блок "Удобства работы"',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'convenience-work',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок "Пункты приема"',
        'menu_slug' => 'map',
        'menu_title' => 'Блок "Пункты приема"',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'map',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
    acf_add_options_page(array(
        'page_title' => 'Блок "Реквизиты"',
        'menu_slug' => 'requisites',
        'menu_title' => 'Блок "Реквизиты"',
        'capability' => 'edit_posts',
        'position' => '',
        'parent_slug' => '',
        'icon_url' => '',
        'redirect' => true,
        'post_id' => 'requisites',
        'autoload' => true,
        'update_button' => 'Обновить',
        'updated_message' => 'Сохранено',
    ));
}

function cw_post_type() {

    register_post_type( 'type_metal',
        array(
            'labels' => array(
                'name' => __( 'Тип металла' ),
                'singular_name' => __( 'Тип металла' )
            ),
            'has_archive' => false,
            'public' => true,
            'rewrite' => array('slug' => 'type-metal'),
            'show_in_rest' => true,
            'supports' => array('editor', 'revisions', 'title', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'page-attributes', 'post-formats')

        )
    );

    register_post_type( 'services',
        array(
            'labels' => array(
                'name' => __( 'Услуги' ),
                'singular_name' => __( 'Услуги' )
            ),
            'public' => true,
            'has_archive' => 'services',
            'rewrite' => array('slug' => 'services'),
            'show_in_rest' => true,
            'hierarchical'          => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',

            'supports' => array('editor', 'revisions', 'title', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'page-attributes', 'post-formats')

        )
    );

    register_post_type( 'autopark',
        array(
            'labels' => array(
                'name' => __( 'Автопарк' ),
                'singular_name' => __( 'Автопарк' )
            ),
            'has_archive' => false,
            'public' => true,
            'rewrite' => array('slug' => 'autopark'),
            'show_in_rest' => true,
            'supports' => array('editor', 'revisions', 'title', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'page-attributes', 'post-formats')

        )
    );

    register_post_type( 'faq',
        array(
            'labels' => array(
                'name' => __( 'Частые вопросы' ),
                'singular_name' => __( 'Частые вопросы' )
            ),
            'has_archive' => false,
            'public' => true,
            'rewrite' => array('slug' => 'faq'),
            'show_in_rest' => true,
            'supports' => array('editor', 'revisions', 'title', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'page-attributes', 'post-formats')

        )
    );

    register_post_type( 'reviews',
        array(
            'labels' => array(
                'name' => __( 'Отзывы' ),
                'singular_name' => __( 'Отзывы' )
            ),
            'has_archive' => false,
            'public' => true,
            'rewrite' => array('slug' => 'reviews'),
            'show_in_rest' => true,
            'supports' => array('editor', 'revisions', 'title', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'page-attributes', 'post-formats')

        )
    );

}
add_action( 'init', 'cw_post_type' );


add_action( 'after_setup_theme', 'tailpress_setup' );


function tailpress_nav_menu_add_li_class( $classes, $item, $args, $depth ) {
	if ( isset( $args->li_class ) ) {
		$classes[] = $args->li_class;
	}

	if ( isset( $args->{"li_class_$depth"} ) ) {
		$classes[] = $args->{"li_class_$depth"};
	}

	return $classes;
}
add_filter( 'nav_menu_css_class', 'tailpress_nav_menu_add_li_class', 10, 4 );

function tailpress_nav_menu_add_submenu_class( $classes, $args, $depth ) {
	if ( isset( $args->submenu_class ) ) {
		$classes[] = $args->submenu_class;
	}

	if ( isset( $args->{"submenu_class_$depth"} ) ) {
		$classes[] = $args->{"submenu_class_$depth"};
	}

	return $classes;
}
add_filter( 'nav_menu_submenu_css_class', 'tailpress_nav_menu_add_submenu_class', 10, 3 );

add_action( 'wp_ajax_my_action', 'my_action_callback' );
add_action( 'wp_ajax_nopriv_my_action', 'my_action_callback' );
function my_action_callback() {
    $whatever = intval( $_POST['whatever'] );
     echo json_encode($whatever);

    die();
}

add_action( 'wp_ajax_get_posts_and_acf', 'get_posts_and_acf_callback' );
add_action( 'wp_ajax_nopriv_get_posts_and_acf', 'get_posts_and_acf_callback' );
function get_posts_and_acf_callback() {

    $exclude = $_POST['exclude'];
    $post_type = $_POST['post_type'];
    $per_page = $_POST['per_page'];

    $temp_services = array();
    $services = get_posts( array(
        'numberposts' => $per_page,
        'orderby'     => 'date',
        'order'       => 'ASC',
        'post_type'   => $post_type,
        'exclude'     => $exclude,
        'suppress_filters' => true,
    ) );

    foreach ($services as $key => $service){
        $temp_services[$key] = (array)$service;
        $temp_services[$key]['acf'] = get_fields($service->ID);
    }
    $services = $temp_services;


    echo json_encode($services);

    wp_die();
}
