<?php get_header('index'); ?>

<?
$post_blocks = parse_blocks( get_the_content( '', false, get_the_ID() ) );

$new_post_blocks = array();
foreach ($post_blocks as $key=>$block){
    if($block['blockName'] != ''){
        $new_post_blocks[] = $block;
    }
}
$post_blocks = $new_post_blocks;

foreach ($post_blocks as $key=>$block){

    $key_last = array_key_last($post_blocks);

    $acf_block = strpos($block["blockName"], 'acf/');
    $core_block = strpos($block["blockName"], 'core/');

    if($key != 0){
        $acf_block_prev = strpos($post_blocks[$key-1]["blockName"], 'acf/');
        $core_block_prev = strpos($post_blocks[$key-1]["blockName"], 'core/');
    }
    if($key != $key_last){
        $acf_block_next = strpos($post_blocks[$key+1]["blockName"], 'acf/');
        $core_block_next = strpos($post_blocks[$key+1]["blockName"], 'core/');
    }

    global $h1_exist;

    if ($acf_block !== false) {
        echo render_block($block);
    }else{

        // Если первый блок и следующий тоже не-acf
        if($key == 0 && $acf_block_next === false) {?>
            <section class="py-5">
            <div class="container py-2 px-5 mx-auto">
            <?=render_block($block)?>
        <?}
        // Если последний блок и предыдущий блок не-acf
        elseif ($key == $key_last && $acf_block_prev === false){?>
            <?=render_block($block)?>
            </div>
            </section>
        <?}
        // Если не последний блок и не первый блок
        else{
            // Если предыдущий блок acf и следующий блок не-acf
            if($acf_block_prev !== false && $acf_block_next === false){?>
                <section class="py-5">
                <div class="container py-2 px-5 mx-auto">
                <?=render_block($block)?>
            <?}
            // Если предыдущий блок не-acf и следующий блок не-acf
            elseif ($acf_block_prev === false && $acf_block_next === false){?>
                <?=render_block($block)?>
            <?}
            // Если предыдущий блок не-acf и следующий блок acf
            elseif ($acf_block_prev === false && $acf_block_next !== false){?>
                <?=render_block($block)?>
                </div>
                </section>
                <?
            }
        }

    }
}
?>

<?php get_footer(); ?>
