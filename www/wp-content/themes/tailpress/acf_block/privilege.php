<?php
/**
 * Acf block: Преимущества
 */

/** @var array $block */

$block_data = array();
$varName = 'columns';

if($block['data']['mode'] == 'local'){
    $block_data = $block['data'];

    $count_steps = $block_data[$varName];
    $block_data[$varName] = array();
    $i = 0;
    while ($i < $count_steps){
        $block_data[$varName][$i]['icon'] = $block_data[$varName.'_'.$i.'_icon'];
        $block_data[$varName][$i]['title'] = $block_data[$varName.'_'.$i.'_title'];
        $block_data[$varName][$i]['desc'] = $block_data[$varName.'_'.$i.'_desc'];
        $i++;
    }

    foreach ($block_data[$varName] as $key => $step){
        $image = wp_get_attachment_image_src($step['icon'], 'thumbnail');

        $block_data[$varName][$key]['icon'] = array();

        $block_data[$varName][$key]['icon']['url'] = $image[0];
        $block_data[$varName][$key]['icon']['width'] = $image[1];
        $block_data[$varName][$key]['icon']['height'] = $image[2];
    }

}
elseif ($block['data']['mode'] == 'general'){
    $block_data = get_fields( 'privilege' );

    foreach ($block_data[$varName] as $key => $step){
        $block_data[$varName][$key]['icon']['url'] = $step['icon']['sizes']['thumbnail'];
        $block_data[$varName][$key]['icon']['width'] = $step['icon']['sizes']['thumbnail-width'];
        $block_data[$varName][$key]['icon']['height'] = $step['icon']['sizes']['thumbnail-height'];
    }

}
?>

<section class="py-5" data-id="<?=$block['id']?>">
    <div class="container py-2 px-5 mx-auto">
        <?if(!empty($block_data['title'])){?>
            <h1 class="text-center"><?=$block_data['title']?></h1>
        <?}?>
        <div class="flex flex-row flex-wrap text-center">
            <?foreach ($block_data[$varName] as $key => $item){?>
                <div class="w-full mx-auto lg:w-1/4 mb-4 px-3">
                    <div class="bg-white shadow-around h-24 w-24 rounded-full p-5 mb-4 mx-auto flex content-center items-center">
                        <img src="<?=$item['icon']['url']?>" alt="Icon-<?=$key?>" width="55" height="60" class="mx-auto block">
                    </div>
                    <?if(!empty($item['title'])){?>
                        <span class="block text-xl pf-font-medium mb-3">
                            <?=$item['title']?>
                        </span>
                    <?}?>
                    <?if(!empty($item['desc'])){?>
                        <p><?=$item['desc']?></p>
                    <?}?>
                </div>
            <?}?>
        </div>
    </div>
</section>
