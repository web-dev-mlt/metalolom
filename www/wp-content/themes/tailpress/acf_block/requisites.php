<?php
/**
 * Acf block: Реквизиты
 */

/** @var array $block */

$block_data = get_fields('requisites');
?>

<section>
    <div class="container py-2 px-5 mx-auto">
        <h1 class="text-5xl text-center pf-font-bold mb-5"><?=$block_data['title']?></h1>
        <div class="text-3xl pf-font-bold text-center mb-4">
            <?=$block_data['sub-title']?>
        </div>
        <div class="flex flex-row flex-wrap justify-between">
            <?foreach ($block_data['columns'] as $column){?>
                <div class="w-full sm:w-6/12 lg:w-3/12 mb-4 px-3">
                    <?=$column['text']?>
                </div>
            <?}?>
        </div>
    </div>
</section>
