<?php
/**
 * Acf block: Слайдер услуг
 */

/** @var array $block */

$services = get_posts( array(
    'numberposts' => -1,
    'orderby'     => 'date',
    'order'       => 'ASC',
    'post_type'   => 'services',
    'suppress_filters' => true,
) );?>
<section class="py-5" data-id="<?=$block['id']?>">
    <div class="container px-7 mx-auto">
        <h1 class="text-center"><?=$block['data']['title']?></h1>
        <div class="metals-items">
        <?foreach( $services as $post ){
            setup_postdata($post);
            $acf = get_fields($post->ID);?>
            <div class="metal-item lg:w-auto relative bg-mlgreen rounded-lg my-3">
                <div class="flex align-center justify-center rounded-md h-full relative overlay px-3"
                     style="background: url(<?=$acf['photo']['sizes']['thumbnail']?>) no-repeat center;">
                    <a href="<?=get_the_permalink($post->ID)?>" class="w-full flex justify-center font-regular font-bold items-center h-full text-white hover:underline hover:text-mlgreen">
                        <?=get_the_title($post->ID)?>
                    </a>
                </div>
            </div>
        <?}
        wp_reset_postdata();
        ?>
        </div>
    </div>
</section>
