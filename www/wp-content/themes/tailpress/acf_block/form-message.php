<?php
/**
 * Acf block: Форма Отправьте сообщение
 */

/** @var array $block */

?>

<section class="py-5 overlay-section-white flex content-center align-center lazy-bg" style="background-image: url('<?=get_template_directory_uri()?>/images/bg-form.jpg')">
    <div class="container py-2 sm:px-5 mx-auto">
        <div class="text-5xl font-regular font-bold text-center mb-5">Отправьте сообщение</div>
        <div class="text-3xl font-regular font-bold text-center mb-3">и мы обязательно ответим</div>

        <?=do_shortcode('[fluentform id="9"]')?>
    </div>
</section>

