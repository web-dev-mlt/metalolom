<?php

/**
 * Acf block: Гарантии приема
 */

/** @var array $block */

$data = get_fields( 'guarantee' );
?>

<section class="py-5 section-text" data-id="<?=$block['id']?>">
    <div class="container sm:px-5 mx-auto">
        <div class="flex flex-col lg:flex-row flex-wrap shadow-around">
            <div class="w-full lg:w-8/12 p-5">
                <h1><?=$data['title']?></h1>
                <?=$data['desc']?>
            </div>
            <div class="hidden lg:block w-full lg:w-4/12"><img src="<?=get_template_directory_uri()?>/images/garantii.png" width="412" height="360" alt="garantii" class="block" style="margin-top: -4rem"></div>
        </div>
        <div class="bg-mgray p-5 px-7">
            <p class="text-mlblue text-lg">
                Для уточнения деталей вы можете обратиться к своему менеджеру – он проконсультирует и оперативно урегулирует все возникшие вопросы. При возникновении любых трудностей при приемке, вывозе металлолома или демонтаже – сообщите нам об этом.
            </p>
            <button class="modal-open mt-4 mx-auto bg-mblue text-lg font-regular font-medium text-white hover:bg-blue-500 shadow-blue px-9 py-3">Связаться с руководством</button>
        </div>
    </div>
</section>
