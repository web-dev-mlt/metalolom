<?php
/**
 * Acf block: Форма оценки
 */

/** @var array $block */

$options = get_fields('options');

$block_data = array();
if($block['data']['mode'] == 'local'){
    $block_data = $block['data'];

    $image_bg = wp_get_attachment_image_src($block_data['background'], 'full');

    $block_data['background'] = array();
    $block_data['background']['url'] = $image_bg[0];
    $block_data['background']['width'] = $image_bg[1];
    $block_data['background']['height'] = $image_bg[2];

}
elseif ($block['data']['mode'] == 'general'){
    $block_data = get_fields( 'form_assessment' );

    $block_data['background']['url'] = $block_data['background']['sizes']['2048x2048'];
    $block_data['background']['width'] = $block_data['background']['sizes']['2048x2048-width'];
    $block_data['background']['height'] = $block_data['background']['sizes']['2048x2048-height'];

}
?>

<section class="py-5 overlay-section-white-half flex content-center align-center lazy-bg"
         style="background-image: url('<?=$block_data['background']['url']?>')" data-id="<?=$block['id']?>">
    <div class="container py-2 px-5 mx-auto">
        <div class="w-full lg:w-6/12">
            <h1 class="text-5xl pf-font-medium mb-5"><?=$block_data['title']?></h1>
            <p>
                <?=$block_data['desc']?>
            </p>

            <?=do_shortcode('[fluentform id="6"]')?>

            <p>Также вы можете переслать фото в популярных мессенджерах.</p>

            <div class="flex flex-col sm:flex-row flex-wrap">
                <div class="w-full mb-2 text-center flex flex-row content-center align-center flex-wrap lg:text-left lg:justify-start lg:w-auto">
                    <?foreach ($options['messengers'] as $messenger){?>
                        <a href="<?=$messenger['link']?>" class="w-auto text-mlgreen text-2xl text-lg pf-font-medium no-underline p-2 py-1">
                            <i class="<?=$messenger['icon']?> shadow-mlgreen rounded-full"></i>
                        </a>
                    <?}?>
                    <a href="<?=$options['phone']->uri()?>" class="w-auto text-mdblue hover:text-mlgreen font-bold text-3xl pf-font-medium no-underline">
                        <?=$options['phone']->international()?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
