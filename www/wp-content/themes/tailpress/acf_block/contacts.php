<?php
/**
 * Acf block: Контакты
 */

/** @var array $block */

$block_data = array();
$block_data = $block['data'];
$varName = 'kontakty_columns';
$count_columns = $block_data[$varName];
$i = 0;
while ($i < $count_columns){

    $varName_2 = $varName.'_'.$i.'_links';
    $count_links = $block_data[$varName_2];
    $j = 0;
    while ($j < $count_links) {
        $block_data['columns'][$i]['links'][$j]['title'] = $block_data[ $varName_2.'_'.$j.'_title' ];
        $block_data['columns'][$i]['links'][$j]['link'] = $block_data[ $varName_2.'_'.$j.'_link' ];

        $j ++;
    }

    $block_data['columns'][$i]['address'] = $block_data[$varName.'_'.$i.'_address'];
    $block_data['columns'][$i]['metro'] = $block_data[$varName.'_'.$i.'_metro'];
    $block_data['columns'][$i]['map'] = $block_data[$varName.'_'.$i.'_map'];

    $i++;

}
?>



<section>
    <div class="container py-2 px-5 mx-auto">
        <h1 class="text-5xl text-center pf-font-bold mb-5">Наши контакты</h1>
        <div class="flex flex-row flex-wrap justify-between">
            <?foreach ($block_data['columns'] as $column){?>
                <div class="flex flex-col lg:pr-3 w-full lg:w-1/2 mb-7">
                    <div class="text-xl pf-font-medium mb-5">
                        <?=$column['address']?>
                    </div>
                    <div class="flex-1 mb-5">
                        <p>Ссылки для навигаторов:
                            <?foreach ($column['links'] as $key => $link){
                                if($key == 0){?>
                                    <a href="<?=$link['link']?>" class="text-mlgreen pf-font-light text-lg hover:text-black mr-4">
                                        <?=$link['title']?>
                                    </a>
                                <?}else{?>
                                    <a href="<?=$link['link']?>" class="text-mlgreen pf-font-light text-lg hover:text-black">
                                        <?=$link['title']?>
                                    </a>
                                <?}
                            }?>
                        </p>
                        <p>
                            <?=$column['metro']?>
                        </p>
                    </div>
                    <button class="modal-open md:w-1/3 bg-mblue text-white hover:bg-blue-500 shadow-blue px-5 py-2 mb-7">Оставить заявку</button>
                    <div class="w-full">
                        <?=do_shortcode($column['map'])?>
                    </div>
                </div>
            <?}?>
        </div>
    </div>
</section>
