<?php
/**
 * Acf block: Таблица услуг
 */

/** @var array $block */

$count_on_page = 3;
$services = get_posts( array(
    'numberposts' => $count_on_page,
    'orderby'     => 'date',
    'order'       => 'ASC',
    'post_type'   => 'services',
    'suppress_filters' => true,
) );

$services_count_all = (wp_count_posts('services')->publish);
$services_count = (wp_count_posts('services')->publish) - $count_on_page;
?>

<section class="py-5 table-services-js" data-id="<?=$block['id']?>">
    <div class="container px-5 mx-auto">
        <h1 class="text-center"><?=$block['data']['title']?></h1>
        <h3 class="text-center">
            Цены актуальные на
            <?
            $date = new DateTime('NOW');
            if($block['data']['actual-date'] == 'static'){
                $date = DateTime::createFromFormat('Ymd', $block['data']['static-date']);
            }
            elseif ($block['data']['actual-date'] == 'now'){
                $date = new DateTime('NOW');
            }

            echo $date->format('d.m.Y');
            ?>
        </h3>
        <div class="table-auto overflow-x-auto shadow-lg">
            <table>
                <thead>
                    <tr>
                        <th>Фото</th>
                        <th>Требования к лому</th>
                        <th>Засор</th>
                        <th>Количество</th>
                        <th>Цена за тонну</th>
                    </tr>
                </thead>
                <tbody data-all-count="<?=$services_count_all?>" data-per-page="<?=$count_on_page?>" data-current-count="<?=$count_on_page?>">
                    <?foreach( $services as $post ){
                        setup_postdata($post);
                        $acf = get_fields($post->ID);
                        ?>
                        <tr data-post-id="<?=$post->ID?>" data-type-post="<?=$post->post_type?>">
                            <td class="p-4">
                                <img src="<?=$acf['photo']['sizes']['thumbnail']?>" alt="<?=$acf['photo']['name']?>" class="block mx-auto" width="<?=$acf['photo']['sizes']['thumbnail-width']?>" height="<?=$acf['photo']['sizes']['thumbnail-height']?>">
                            </td>
                            <td>
                                <?=$acf['requirement']?>
                            </td>
                            <td class="font-light text-center">
                                <?=number_format($acf['zasor'], 1)?>%
                            </td>
                            <td>
                                <div class="flex flex-row justify-between p-2 w-full rounded-lg relative bg-transparent mt-1 border-2 border-mgray">
                                    <button data-action="decrement" class="h-8 w-8 rounded-full text-white hover:text-mlgreen bg-mblue h-8 w-8 cursor-pointer outline-none">
                                        <span class="m-auto text-2xl font-thin">−</span>
                                    </button>
                                    <input type="number" class="outline-none focus:outline-none text-center w-14 font-semibold text-md hover:text-black focus:text-black md:text-basecursor-default flex items-center text-gray-700 outline-none" name="custom-input-number" value="1">
                                    <button data-action="increment" class="h-8 w-8 rounded-full text-white hover:text-mlgreen bg-mblue h-8 w-8 cursor-pointer">
                                        <span class="m-auto text-2xl font-thin">+</span>
                                    </button>
                                </div>
                            </td>
                            <td class="font-bold text-center text-xl price-in-table" data-price="<?=$acf['price']?>">
                                от <?=$acf['price']?> руб
                            </td>
                        </tr>

                    <?}
                    wp_reset_postdata();
                    ?>
                </tbody>
            </table>
        </div>
        <?if($services_count > 0){?>
            <div class="flex justify-center w-full">
                <button class="border border-mlgreen text-mlgreen hover:text-black px-4 py-2 mt-7 w-60 table-btn-more-js">
                    Загрузить ещё
                </button>
            </div>
        <?}?>
    </div>
</section>

