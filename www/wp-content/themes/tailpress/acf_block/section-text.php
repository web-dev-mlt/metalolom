<?php
/**
 * Acf block: Текстовый блок
 */
?>

<section class="section-text py-5" data-id="<?=$block['id']?>">
    <div class="container mx-auto px-5">
        <?=$block['data']['text']?>
    </div>
</section>
