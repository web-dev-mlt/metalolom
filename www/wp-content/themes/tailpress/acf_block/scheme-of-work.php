<?php
/**
 * Acf block: Схема работы
 */

/** @var array $block */

$block_data = array();

if($block['data']['mode'] == 'local'){
    $block_data = $block['data'];

    $image_bg = wp_get_attachment_image_src($block_data['background'], 'full');

    $block_data['background'] = array();
    $block_data['background']['url'] = $image_bg[0];
    $block_data['background']['width'] = $image_bg[1];
    $block_data['background']['height'] = $image_bg[2];


    $varName = 'steps';

    $count_steps = $block_data[$varName];
    $block_data[$varName] = array();
    $i = 0;
    while ($i < $count_steps){
        $block_data[$varName][$i]['icon'] = $block_data[$varName.'_'.$i.'_icon'];
        $block_data[$varName][$i]['desc'] = $block_data[$varName.'_'.$i.'_desc'];
        $i++;
    }

    foreach ($block_data['steps'] as $key => $step){
        $image = wp_get_attachment_image_src($step['icon'], 'thumbnail');

        $block_data['steps'][$key]['icon'] = array();

        $block_data['steps'][$key]['icon']['url'] = $image[0];
        $block_data['steps'][$key]['icon']['width'] = $image[1];
        $block_data['steps'][$key]['icon']['height'] = $image[2];
    }
    
}
elseif ($block['data']['mode'] == 'general'){
    $block_data = get_fields( 'scheme_of_work' );

    $block_data['background']['url'] = $block_data['background']['sizes']['2048x2048'];
    $block_data['background']['width'] = $block_data['background']['sizes']['2048x2048-width'];
    $block_data['background']['height'] = $block_data['background']['sizes']['2048x2048-height'];

    foreach ($block_data['steps'] as $key => $step){
        $block_data['steps'][$key]['icon']['url'] = $step['icon']['sizes']['thumbnail'];
        $block_data['steps'][$key]['icon']['width'] = $step['icon']['sizes']['thumbnail-width'];
        $block_data['steps'][$key]['icon']['height'] = $step['icon']['sizes']['thumbnail-height'];
    }

}
?>

<section class="overlay-section-white py-5 lazy-bg" data-id="<?=$block['id']?>" style="background-image: url('<?=$block_data['background']['url']?>')">
    <div class="container mx-auto px-5">
        <h1 class="text-center"><?=$block_data['title']?></h1>
        <div class="w-full py-6">
            <div class="flex flex-col md:flex-row">
                <?foreach ($block_data['steps'] as $key => $step){?>
                    <div class="w-full mb-5 md:mb-0 md:w-1/5">
                        <div class="mb-5">
                            <img src="<?=$step['icon']['url']?>" height="<?=$step['icon']['height']?>" width="<?=$step['icon']['width']?>" alt="work-<?=$key?>" class="h-10 w-10 mx-auto block">
                        </div>

                        <?if($key == 0){?>
                            <div class="relative mb-2">
                                <div class="absolute flex align-center items-center align-middle content-center line-half-left">
                                    <div class="w-full bg-mavgray rounded items-center align-middle align-center flex-1">
                                        <div class="w-0 bg-green-300 px-1 md:py-1 rounded" style="width: 50%;"></div>
                                    </div>
                                </div>
                                <div class="w-10 h-10 mx-auto bg-mdgray border-4 border-white rounded-full text-lg text-white flex items-center">
                                    <span class="text-center font-bold text-white w-full"><?=str_pad($key+1, 2, '0', STR_PAD_LEFT);?></span>
                                </div>
                            </div>
                        <?}
                        elseif($key == (count($block_data['steps']) - 1) ){?>
                            <div class="relative mb-2">
                                <div class="absolute flex align-center items-center align-middle content-center line-full">
                                    <div class="w-full bg-mavgray rounded items-center align-middle align-center flex-1">
                                        <div class="w-0 bg-green-300 px-1 md:py-1 rounded" style="width: 100%;"></div>
                                    </div>
                                </div>
                                <div class="w-10 h-10 mx-auto bg-mdgray border-4 border-white rounded-full text-lg text-white flex items-center">
                                    <span class="text-center font-bold text-white w-full"><?=str_pad($key+1, 2, '0', STR_PAD_LEFT);?></span>
                                </div>
                                <div class="absolute flex align-center items-center align-middle content-center line-half-right">
                                    <div class="w-full bg-mavgray rounded items-center align-middle align-center flex-1">
                                        <div class="w-0 bg-green-300 px-1 md:py-1 rounded" style="width: 50%;"></div>
                                    </div>
                                </div>
                            </div>
                        <?}
                        else{?>
                            <div class="relative mb-2">
                                <div class="absolute flex align-center items-center align-middle content-center line-full">
                                    <div class="w-full bg-mavgray rounded items-center align-middle align-center flex-1">
                                        <div class="w-0 bg-green-300 px-1 md:py-1 rounded" style="width: 100%;"></div>
                                    </div>
                                </div>
                                <div class="w-10 h-10 mx-auto bg-mdgray border-4 border-white rounded-full text-lg text-white flex items-center">
                                    <span class="text-center font-bold text-white w-full"><?=str_pad($key+1, 2, '0', STR_PAD_LEFT);?></span>
                                </div>
                            </div>
                        <?}?>

                        <div class="text-xs text-center md:text-base">
                            <?=$step['desc']?>
                        </div>
                    </div>
                <?}?>
            </div>
        </div>
    </div>
</section>
