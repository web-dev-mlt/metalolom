<?php
/**
 * Acf block: Удобства работы
 */

/** @var array $block */

$block_data = array();

if($block['data']['udobstva_raboty_mode'] == 'local'){
    $block_data = $block['data'];
    
    $varName = 'udobstva_raboty_steps';

    $count_steps = $block_data[$varName];
    $block_data[$varName] = array();
    $i = 0;
    while ($i < $count_steps){
        $block_data['steps'][$i]['desc'] = $block_data[$varName.'_'.$i.'_desc'];
        $i++;
    }

    $block_data['title'] = $block['data']['udobstva_raboty_title'];

}
elseif ($block['data']['udobstva_raboty_mode'] == 'general'){
    $block_data = get_fields( 'convenience-work' );
}
?>

<section class="py-5">
    <div class="container py-2 px-5 mx-auto">
        <?if(!empty($block_data['title'])){?>
            <h1 class="text-center"><?=$block_data['title']?></h1>
        <?}?>
        <div class="flex flex-row flex-wrap text-center">
            <?if(!empty($block_data['steps'])){
                foreach ($block_data['steps'] as $key => $item){?>
                    <div class="w-full mx-auto lg:w-1/4 mb-4 px-3">
                        <div class="bg-white shadow-around h-24 w-24 rounded-full p-5 mb-4 text-center text-5xl text-mlgreen font-bold font-regular mx-auto flex content-center items-center">
                            <?=str_pad($key+1, 2, '0', STR_PAD_LEFT);?>
                        </div>
                        <p>
                            <?=$item['desc']?>
                        </p>
                    </div>
                <?}
            }?>
        </div>
    </div>
</section>
