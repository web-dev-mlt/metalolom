<?php

/**
 * Acf block: Карта пунктов приема
 */

/** @var array $block */

$services = get_fields('map');

?>

<section class="py-5 pb-7 bg-mgray">
    <div class="container px-5 mx-auto">
        <h1 class="text-center"><?=$services['block-map']['title']?></h1>
        <img src="<?=$services['block-map']['map']['sizes']['medium_large']?>" width="<?=$services['block-map']['map']['sizes']['medium_large-width']?>" height="<?=$services['block-map']['map']['sizes']['medium_large-height']?>" alt="map" class="block mx-auto">
    </div>
</section>
