<?php
/**
 * Acf block: Частые вопросы
 */

/** @var array $block */

$faq = get_posts( array(
    'numberposts' => -1,
    'orderby'     => 'date',
    'order'       => 'ASC',
    'post_type'   => 'faq',
    'suppress_filters' => true,
) );
?>

<section class="py-5" data-id="<?=$block['id']?>">
    <div class="container py-2 px-5 mx-auto">
        <h1 class="text-5xl text-center pf-font-bold mb-5">Частые вопросы о сдаче металла</h1>
        <div class="w-full mx-auto p-8">
            <div>

                <?foreach( $faq as $key => $post ){
                    setup_postdata($post);?>
                    <div itemscope itemtype="https://schema.org/FAQPage" style="display: none">
                        <div itemprop="mainEntity" itemscope itemtype="https://schema.org/Question">
                            <meta itemprop="name" content="<?=get_the_title($post->ID)?>">
                            <meta itemprop="text" content="<?=get_the_title($post->ID)?>"/>
                            <meta itemprop="answerCount" content="1"/>
                        </div>
                        <meta itemprop="upvoteCount" content="196"/>
                        <div itemprop="acceptedAnswer" itemscope="" itemtype="http://schema.org/Answer">
                            <meta itemprop="upvoteCount" content="3"/>
                            <meta itemprop="text" content="<?=get_the_content($post->ID)?>"/>
                        </div>
                    </div>

                    <div class="tab w-full overflow-hidden border-t shadow-md mb-5">
                        <input class="absolute opacity-0" id="tab-multi-<?=$key?>" type="checkbox" name="according">
                        <label class="block p-5 leading-normal cursor-pointer hover:text-mlgreen" for="tab-multi-<?=$key?>">
                            <?=get_the_title($post->ID)?>
                        </label>
                        <div class="tab-content overflow-hidden border-l-2 border-indigo-500 leading-normal">
                            <p>
                                <?=get_the_content($post->ID)?>
                            </p>
                        </div>
                    </div>
                <?}
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</section>
