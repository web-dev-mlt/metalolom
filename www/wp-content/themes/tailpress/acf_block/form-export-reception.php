<?php
/**
 * Acf block: Форма вывоз и прием
 */

/** @var array $block */

?>

<section class="py-5 overlay-section-white flex content-center align-center lazy-bg" style="background-image: url('<?=get_template_directory_uri()?>/images/bg-form.jpg')" data-id="<?=$block['id']?>">
    <div class="container py-2 px-5 mx-auto">
        <div class="text-5xl font-regular font-bold text-center mb-5">Заказать вывоз и прием металлолома</div>
        <div class="text-3xl font-regular font-bold text-center mb-3">Наш менеджер свяжется с вами в течение 5 минут</div>

        <?=do_shortcode('[fluentform id="7"]')?>
    </div>
</section>
