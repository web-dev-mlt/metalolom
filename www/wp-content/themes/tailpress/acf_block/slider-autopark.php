<?php
/**
 * Acf block: Слайдер автопарка
 */

/** @var array $block */

$services = get_posts( array(
    'numberposts' => -1,
    'orderby'     => 'date',
    'order'       => 'ASC',
    'post_type'   => 'autopark',
    'suppress_filters' => true,
) );?>

<section class="py-5" data-id="<?=$block['id']?>">
    <div class="container px-7 md:px-5 mx-auto">
        <h1 class="text-center">Наш автопарк</h1>
        <div class="park-items text-center">
            <?foreach( $services as $post ){
                setup_postdata($post);
                $acf = get_fields($post->ID);?>
                <div>
                    <div class="mb-4">
                        <img data-lazy="<?=$acf['image']['sizes']['medium']?>" width="250" height="160" alt="<?=$acf['image']['name']?>" class="block object-contain mx-auto h-40 lazy-off">
                    </div>
                    <a class="mb-4 text-xl font-regular text-center uppercase text-black font-bold text-mlgreen hover:underline" href="<?=get_the_permalink($post->ID)?>">
                        <?=get_the_title($post->ID)?>
                    </a>
                    <p>
                        <?=$acf['desc']?>
                    </p>
                </div>
            <?}
            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>
