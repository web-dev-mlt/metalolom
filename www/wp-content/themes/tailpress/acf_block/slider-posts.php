<?php
/**
 * Acf block: Слайдер постов(услуг)
 */

/** @var array $block */


global $post_type;

$arIds = Page_For_Post_Type::get_page_ids();
$post_id = get_the_ID();

if (in_array($post_id, $arIds)){
    $post_type = array_search($post_id, $arIds);
}

if (isset($post_type)){

    $items = get_posts( array(
        'numberposts' => -1,
        'orderby'     => 'date',
        'order'       => 'ASC',
        'post_type'   => $post_type,
        'suppress_filters' => true,
    ) );
    ?>
    <section class="py-5">
        <div class="container px-7 md:px-5 mx-auto">
            <div class="examples-items">
                <?foreach( $items as $post ){
                    setup_postdata($post);?>
                    <div>
                        <div class="mb-4">
                            <img data-lazy="<? if(has_post_thumbnail($post->ID)){
                                $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(280,180));
                                echo $img[0];
                            }else{?>
                                <?=get_template_directory_uri()?>/images/reviews1.png
                            <?} ?>" alt="<?=get_the_title($post->ID)?> img" class="block object-center" width="280" height="180">
                        </div>
                        <a class="mb-4 text-xl font-regular text-black font-bold text-mlgreen hover:underline" href="<?=get_the_permalink($post->ID)?>">
                            <?=get_the_title($post->ID)?>
                        </a>
                        <p>
                            <?=get_the_excerpt($post->ID)?>
                        </p>
                    </div>
                <?}
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </section>
<?}?>

