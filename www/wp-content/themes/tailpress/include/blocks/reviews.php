<?
$reviews = get_posts( array(
    'numberposts' => -1,
    'orderby'     => 'date',
    'order'       => 'ASC',
    'post_type'   => 'reviews',
    'suppress_filters' => true,
) );
?>

<section>
    <div class="container py-2 px-7 sm:px-5 mx-auto">
        <h1 class="text-5xl text-center pf-font-bold mb-5">Отзывы наших клиентов</h1>
        <div class="reviews-items">
            <?foreach( $reviews as $post ){
                setup_postdata($post);?>
                <div class="flex flex-col px-2">
                    <div class="flex justify-center mb-5">
                        <img src="<? if(has_post_thumbnail($post->ID)){
                            $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(165,165));
                            echo $img[0];
                        }else{?>
                          <?=get_template_directory_uri()?>/images/reviews1.png
                        <?} ?>" alt="Отзыв" class="block object-center" width="165" height="165">

                    </div>
                    <span class="block pf-font-bold text-lg text-center mb-5"><?=get_the_title($post->ID)?></span>
                    <p class="text-center">
                        <?the_excerpt($post->ID)?>
                    </p>
                </div>
            <?}
            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>
