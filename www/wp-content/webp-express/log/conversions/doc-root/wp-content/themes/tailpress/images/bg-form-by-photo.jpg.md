WebP Express 0.20.1. Conversion triggered using bulk conversion, 2021-08-13 15:46:36

*WebP Convert 2.6.0*  ignited.
- PHP version: 7.4.22
- Server software: Apache/2.4.29

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/tailpress/images/bg-form-by-photo.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/bg-form-by-photo.jpg.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- auto-limit: true
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- encoding: "lossy"
- metadata: "all"
- quality: 20
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/tailpress/images/bg-form-by-photo.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/bg-form-by-photo.jpg.webp
- encoding: "lossy"
- low-memory: true
- log-call-arguments: true
- metadata: "all"
- method: 6
- quality: 20
- use-nice: true
- try-common-system-paths: true
- try-supplied-binary-for-os: true
- command-line-options: ""

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-limit: true
- auto-filter: false
- default-quality: 75
- max-quality: 85
- near-lossless: 60
- preset: "none"
- size-in-percentage: null (not set)
- sharp-yuv: true
- skip: false
- try-cwebp: true
- try-discovering-cwebp: true
- rel-path-to-precompiled-binaries: *****
- skip-these-precompiled-binaries: ""
------------

Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *0.6.1*
We could get the version, so yes, a plain cwebp call works (spent 7 ms)
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 1 binaries (spent 5 ms)
- /usr/bin/cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries (spent 0 ms)
- /usr/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 4
Found 4 binaries (spent 0 ms)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-061-linux-x86-64
Discovering cwebp binaries took: 13 ms

Detecting versions of the cwebp binaries found (except supplied binaries)
- Executing: cwebp -version 2>&1. Result: version: *0.6.1*
- Executing: /usr/bin/cwebp -version 2>&1. Result: version: *0.6.1*
Detecting versions took: 9 ms
Binaries ordered by version number.
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64: (version: 1.2.0)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64: (version: 1.1.0)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static: (version: 1.0.3)
- cwebp: (version: 0.6.1)
- /usr/bin/cwebp: (version: 0.6.1)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-061-linux-x86-64: (version: 0.6.1)
Starting conversion, using the first of these. If that should fail, the next will be tried and so on.
Checking checksum for supplied binary: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64
Checksum test took: 18 ms
Creating command line options for version: 1.2.0
Running auto-limit
Quality setting: 20. 
Quality of jpeg: 91. 
Auto-limit result: 20 (no limiting needed this time).
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64 -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/bg-form-by-photo.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/bg-form-by-photo.jpg.webp' 2>&1

*Output:* 
nice: '[doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64': Permission denied

Executing cwebp binary took: 7 ms

Exec failed (return code: 126)
Note: You can prevent trying this precompiled binary, by setting the "skip-these-precompiled-binaries" option to "cwebp-120-linux-x86-64"
Checking checksum for supplied binary: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64
Checksum test took: 15 ms
Creating command line options for version: 1.1.0
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64 -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/bg-form-by-photo.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/bg-form-by-photo.jpg.webp' 2>&1

*Output:* 
nice: '[doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64': Permission denied

Executing cwebp binary took: 6 ms

Exec failed (return code: 126)
Note: You can prevent trying this precompiled binary, by setting the "skip-these-precompiled-binaries" option to "cwebp-110-linux-x86-64"
Checking checksum for supplied binary: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static
Checksum test took: 20 ms
Creating command line options for version: 1.0.3
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/bg-form-by-photo.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/bg-form-by-photo.jpg.webp' 2>&1

*Output:* 
nice: '[doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static': Permission denied

Executing cwebp binary took: 5 ms

Exec failed (return code: 126)
Note: You can prevent trying this precompiled binary, by setting the "skip-these-precompiled-binaries" option to "cwebp-103-linux-x86-64-static"
Creating command line options for version: 0.6.1
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/bg-form-by-photo.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/bg-form-by-photo.jpg.webp' 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/bg-form-by-photo.jpg.webp'
File:      [doc-root]/wp-content/themes/tailpress/images/bg-form-by-photo.jpg
Dimension: 4672 x 3488
Output:    248308 bytes Y-U-V-All-PSNR 37.59 38.34 38.77   37.88 dB
block count:  intra4: 18674
              intra16: 44982  (-> 70.66%)
              skipped block: 29995 (47.12%)
bytes used:  header:            258  (0.1%)
             mode-partition:  91363  (36.8%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   94507 |    1601 |     967 |     570 |   97645  (39.3%)
 intra16-coeffs:  |    5051 |    2543 |    4462 |    6750 |   18806  (7.6%)
  chroma coeffs:  |   32693 |    1303 |    2242 |    3972 |   40210  (16.2%)
    macroblocks:  |      26%|       5%|      13%|      54%|   63656
      quantizer:  |      78 |      70 |      59 |      47 |
   filter level:  |      63 |      63 |      63 |      39 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |  132251 |    5447 |    7671 |   11292 |  156661  (63.1%)
Metadata:
  * ICC profile:    3144 bytes
  * EXIF data:      6320 bytes
  * XMP data:       5553 bytes

Executing cwebp binary took: 4244 ms

Success
cwebp succeeded :)

Converted image in 4580 ms, reducing file size with 83% (went from 1500 kb to 257 kb)
