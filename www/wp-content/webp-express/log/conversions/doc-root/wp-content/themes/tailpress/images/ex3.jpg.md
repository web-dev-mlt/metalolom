WebP Express 0.20.1. Conversion triggered using bulk conversion, 2021-08-13 15:46:30

*WebP Convert 2.6.0*  ignited.
- PHP version: 7.4.22
- Server software: Apache/2.4.29

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/tailpress/images/ex3.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/ex3.jpg.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- auto-limit: true
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- encoding: "lossy"
- metadata: "all"
- quality: 20
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/tailpress/images/ex3.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/ex3.jpg.webp
- encoding: "lossy"
- low-memory: true
- log-call-arguments: true
- metadata: "all"
- method: 6
- quality: 20
- use-nice: true
- try-common-system-paths: true
- try-supplied-binary-for-os: true
- command-line-options: ""

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-limit: true
- auto-filter: false
- default-quality: 75
- max-quality: 85
- near-lossless: 60
- preset: "none"
- size-in-percentage: null (not set)
- sharp-yuv: true
- skip: false
- try-cwebp: true
- try-discovering-cwebp: true
- rel-path-to-precompiled-binaries: *****
- skip-these-precompiled-binaries: ""
------------

Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *0.6.1*
We could get the version, so yes, a plain cwebp call works (spent 8 ms)
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 1 binaries (spent 6 ms)
- /usr/bin/cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries (spent 0 ms)
- /usr/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 4
Found 4 binaries (spent 0 ms)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-061-linux-x86-64
Discovering cwebp binaries took: 15 ms

Detecting versions of the cwebp binaries found (except supplied binaries)
- Executing: cwebp -version 2>&1. Result: version: *0.6.1*
- Executing: /usr/bin/cwebp -version 2>&1. Result: version: *0.6.1*
Detecting versions took: 10 ms
Binaries ordered by version number.
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64: (version: 1.2.0)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64: (version: 1.1.0)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static: (version: 1.0.3)
- cwebp: (version: 0.6.1)
- /usr/bin/cwebp: (version: 0.6.1)
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-061-linux-x86-64: (version: 0.6.1)
Starting conversion, using the first of these. If that should fail, the next will be tried and so on.
Checking checksum for supplied binary: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64
Checksum test took: 18 ms
Creating command line options for version: 1.2.0
Running auto-limit
Quality setting: 20. 
Quality of jpeg: 92. 
Auto-limit result: 20 (no limiting needed this time).
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64 -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/ex3.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/ex3.jpg.webp' 2>&1

*Output:* 
nice: '[doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-120-linux-x86-64': Permission denied

Executing cwebp binary took: 7 ms

Exec failed (return code: 126)
Note: You can prevent trying this precompiled binary, by setting the "skip-these-precompiled-binaries" option to "cwebp-120-linux-x86-64"
Checking checksum for supplied binary: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64
Checksum test took: 16 ms
Creating command line options for version: 1.1.0
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64 -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/ex3.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/ex3.jpg.webp' 2>&1

*Output:* 
nice: '[doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-110-linux-x86-64': Permission denied

Executing cwebp binary took: 7 ms

Exec failed (return code: 126)
Note: You can prevent trying this precompiled binary, by setting the "skip-these-precompiled-binaries" option to "cwebp-110-linux-x86-64"
Checking checksum for supplied binary: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static
Checksum test took: 21 ms
Creating command line options for version: 1.0.3
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/ex3.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/ex3.jpg.webp' 2>&1

*Output:* 
nice: '[doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-linux-x86-64-static': Permission denied

Executing cwebp binary took: 7 ms

Exec failed (return code: 126)
Note: You can prevent trying this precompiled binary, by setting the "skip-these-precompiled-binaries" option to "cwebp-103-linux-x86-64-static"
Creating command line options for version: 0.6.1
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata all -q 20 -alpha_q '85' -sharp_yuv -m 6 -low_memory '[doc-root]/wp-content/themes/tailpress/images/ex3.jpg' -o '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/ex3.jpg.webp' 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/themes/tailpress/images/ex3.jpg.webp'
File:      [doc-root]/wp-content/themes/tailpress/images/ex3.jpg
Dimension: 406 x 264
Output:    5452 bytes Y-U-V-All-PSNR 32.54 35.60 36.53   33.42 dB
block count:  intra4: 293
              intra16: 149  (-> 33.71%)
              skipped block: 21 (4.75%)
bytes used:  header:             59  (1.1%)
             mode-partition:   1201  (22.0%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    3112 |       5 |       8 |       4 |    3129  (57.4%)
 intra16-coeffs:  |     128 |      14 |      19 |     117 |     278  (5.1%)
  chroma coeffs:  |     696 |       8 |       9 |      45 |     758  (13.9%)
    macroblocks:  |      72%|       1%|       2%|      23%|     442
      quantizer:  |      71 |      59 |      46 |      39 |
   filter level:  |      63 |      26 |      38 |      16 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    3936 |      27 |      36 |     166 |    4165  (76.4%)
Metadata:
  * ICC profile:    3144 bytes
  * EXIF data:      6210 bytes
  * XMP data:      11726 bytes

Executing cwebp binary took: 43 ms

Success
cwebp succeeded :)

Converted image in 156 ms, reducing file size with 53% (went from 56 kb to 26 kb)
