WebP Express 0.20.1. Conversion triggered using bulk conversion, 2021-07-22 21:27:34

Converter set to: gd

*WebP Convert 2.6.0*  ignited.
- PHP version: 7.4.21
- Server software: Apache/2.4.25 (Debian)

Gd converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/webp-express/test/dice.png
- destination: [doc-root]/wp-content/webp-express/webp-images/plugins/webp-express/test/dice.png.webp
- log-call-arguments: true
- quality: 85

The following options have not been explicitly set, so using the following defaults:
- auto-limit: true
- default-quality: 85
- max-quality: 85
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- encoding
- metadata
- near-lossless
- skip-pngs
------------

GD Version: 2.2.5
image is true color
Bypassing auto-limit (it is only active for jpegs)
Quality: 85. 

Converted image in 153 ms, reducing file size with 73% (went from 231 kb to 62 kb)
