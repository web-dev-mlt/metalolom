WebP Express 0.20.1. Conversion triggered using bulk conversion, 2021-08-13 15:45:36

Converter set to: imagick

*WebP Convert 2.6.0*  ignited.
- PHP version: 7.4.22
- Server software: Apache/2.4.29

Imagick converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/plugins/webp-express/test/test-pattern-tv.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/plugins/webp-express/test/test-pattern-tv.jpg.webp
- encoding: "lossy"
- log-call-arguments: true
- metadata: "all"
- quality: 20

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-limit: true
- auto-filter: false
- default-quality: 75
- low-memory: false
- max-quality: 85
- method: 6
- preset: "none"
- sharp-yuv: true
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- converters
------------

Running auto-limit
Quality setting: 20. 
Quality of jpeg: 93. 
Auto-limit result: 20 (no limiting needed this time).

Converted image in 64 ms, reducing file size with 85% (went from 28 kb to 4 kb)
