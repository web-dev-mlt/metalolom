<?php
/**
 *
 */
namespace oiyamaps;


function oiym_nag_ignore()
{
	global $current_user;
	$user_id = $current_user->ID;
	/* If user clicks to ignore the notice, add that to their user meta */
	if ( isset($_GET['oiym_nag_ignore']) && $_GET['oiym_nag_ignore'] == '0' )
	{
		add_user_meta($user_id, 'oiym_ignore_notice', 'true', true);
	}
}
add_action('admin_init', __NAMESPACE__.'\oiym_nag_ignore');

// eof
