<?php

/**
 * Plugin Name: Inline JavaScript in Head
 * Description: Boosts performance of critical short JavaScripts by placing their content directly into the HTML head.
 * Version: 1.2.0
 */


namespace InlineJavaScriptInHead;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

class Plugin {

	private static $instance;

	/** @return Plugin */
	public static function instance() {
		if ( Plugin::$instance === null ) {
			Plugin::$instance = new Plugin();
		}
		return Plugin::$instance;
	}

	/**
	 * List of all filter hook of this plugin
	 */
	const FILTER_HANDLES = "inline_javascript_in_footer_handles";
	const FILTER_CSS_HANDLES = "inline_css_in_head_handles";
	const FILTER_WRAP_TRY_CATCH = "inline_javascript_in_head_wrap_try_catch";

	/**
	 * Plugin constructor
	 */
	private function __construct() {
        add_action( 'wp_enqueue_scripts', array( $this, 'deregister_scripts_with_inline' ), 999999 );
        add_action( 'wp_enqueue_scripts', array( $this, 'deregister_styles_with_inline' ), 999999 );
	}

    public function inline_script($handle) {
        $scripts = wp_scripts();
        $script_url = $scripts->registered[ $handle ]->src;
        $script_path = realpath( str_replace( site_url(), '.', $script_url ) );
        $script_content = file_get_contents( $script_path );


        add_action( 'wp_footer', function () use ($script_content, $handle){
            ?>
            <script id="<?=$handle?>"><?=$script_content?></script>
            <?php
        }, PHP_INT_MAX );
    }

    public function deregister_scripts_with_inline(){
        $handles = apply_filters( Plugin::FILTER_HANDLES, array() );
        if ( empty( $handles ) ) {
            return;
        }
        $scripts = wp_scripts();
        if ( empty( $scripts ) || empty( $scripts->registered ) ) {
            return;
        }
        foreach ( $handles as $handle ) {
            $this->inline_script($handle);

            wp_deregister_script($handle);
            wp_dequeue_script($handle);
        }
    }

    public function deregister_scripts(){
        $handles = apply_filters( Plugin::FILTER_HANDLES, array() );
        if ( empty( $handles ) ) {
            return;
        }
        $scripts = wp_scripts();
        if ( empty( $scripts ) || empty( $scripts->registered ) ) {
            return;
        }
        foreach ( $handles as $handle ) {
            wp_deregister_script($handle);
            wp_dequeue_script($handle);
        }
    }

    public function deregister_styles_with_inline(){
        $handles = apply_filters( Plugin::FILTER_CSS_HANDLES, array() );
        if ( empty( $handles ) ) {
            return;
        }
        $styles = wp_styles();
        if ( empty( $styles ) || empty( $styles->registered ) ) {
            return;
        }
        foreach ( $handles as $handle ) {
            $this->inline_style($handle);

            wp_deregister_style($handle);
            wp_dequeue_style($handle);

        }
    }

    public function inline_style($handle) {
        $styles = wp_styles();
        $style_url = $styles->registered[ $handle ]->src;
        $style_path = realpath( str_replace( site_url(), '.', $style_url ) );
        if(empty($style_path)){
           return;
        }
        $style_content = file_get_contents( $style_path );



        add_action( 'wp_head', function () use ($style_content, $handle){
            ?>
            <style id="<?=$handle?>"><?=$style_content?></style>
            <?php
        }, PHP_INT_MAX );
    }






}

Plugin::instance();
