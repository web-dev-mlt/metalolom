=== EmbedPress Pro ===
Contributors: wpdevteam
Tags: EmbedPress, vimeo, youtube, wistia
Requires at least: 4.6
Tested up to: 5.8
Stable tag: 3.1.1
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Extend flexibility from Vimeo, YouTube, Wistia embeds rendered by EmbedPress.

== Description ==
Extend flexibility from Vimeo, youtube, wistia embeds rendered by EmbedPress.

== Installation ==
There're two ways to install EmbedPress Pro plugin:

**Through your WordPress site's admin**

1. Go to your site's admin page;
2. Access the "Plugins" page;
3. Click on the "Add New" button;
4. Search for "EmbedPress Pro";
5. Install EmbedPress Pro plugin;
6. Activate the EmbedPress Pro plugin.

**Manually uploading the plugin to your repository**

1. Download the EmbedPress Pro plugin zip file;
2. Upload the plugin to your site's repository under the *"/wp-content/plugins/"* directory;
3. Go to your site's admin page;
4. Access the "Plugins" page;
5. Activate the EmbedPress pro plugin.

== Usage ==
- Make sure you have EmbedPress plugin installed and active;
- Go to EmbedPress Settings page, click on "Vimeo","Youtube","wistia", tab and customize its options at will;
- That's it.

== Changelog ==

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

= [3.1.1] - 2021-08-05 =
* [Added] YouTube Subscription button for Elementor and Classic and Block Editor
* [Added] Language file for easy translation
* [Bug] Few minor bug fix and improvements.

= [3.1.0] - 2021-07-19 =
* [Bug] Few minor bug fix and improvements.

= [3.0.3] - 2021-06-23 =
* [Added] Boomplay album and playlist Embed
* [Added] Spotify follower widget
* [Bug] Few minor bug fix and improvements.

= [3.0.2] - 2021-06-08 =
* [Added] Branding controls for Vimeo
* [Added] Branding controls for Wistia
* [Added] Branding controls for Twitch
* [Bug] Few minor bug fix and improvements.

= [3.0.1] - 2021-05-27 =
* [Bug] Few minor bug fix and improvements.

= [3.0.0] - 2021-05-19 =

* [Added] Branding preview for Classic and Gutenberg editor
* [Added] Opacity control for custom logo.
* [Bug] Few minor bug fix and improvements.

= [2.4.6] - 2021-04-06 =

* [Added] Twitch live chat option.
* [Added] Youtube custom logo and CTA for Elementor, Gutenberg and Classic editor.
* [Fixed] Custom link attribute on Elementor.
* [Fixed] Wrong text domain on wistia domain.
* [Fixed] Dynamic test domain and translation issue on license page.
* [Bug] Few minor bug fix and improvements.

= [2.4.5] - 2021-03-23 =

* [Fixed] Settings tab broken issue.

= [2.4.4] - 2020-07-22 =

* [Fixed] Added control option  for Dailymotion (elementor)
* [New] New Document Upload Support

= [2.4.3] - 2020-05-10 =

* [Added] Enhanched Alighment Control for all Gutenberg Blocks
* [Added] Docs Link for all Gutenberg Blocks
* [Bug] Minor bug fix

= [2.4.2] - 2020-04-06 =

* [Bug] Fixed - Vimeo video not working in Gutenberg
* Add some control for Vimeo

= [2.4.1] - 2020-03-25 =

* Add SoundCloud control in EmbedPress Elementor widget

= [2.4.0] - 2020-03-15 =

* [Bug] Fixed - Youtube class not found issue
* Add more control in EmbedPress Elementor widget

= [2.3.2] - 2020-03-08 =

* [Bug] Fixed - Plugin does not have a valid header.

= [2.3.1] - 2020-03-04 =

* Add Youtube Gutenberg Block.
* Add Vimeo Gutenberg Block.

= [2.3.0] - 2020-03-02 =

* Initial release.
