<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */


use \Embedpress\Pro\Providers\Vimeo as VimeoPlugin;
use \Embedpress\Pro\Providers\Youtube as YoutubePlugin;
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function embedpress_pro_blocks_cgb_block_assets() { // phpcs:ignore
	// Styles.
	wp_enqueue_style(
		'embedpress_pro-cgb-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-editor' ), // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);
}

// Hook: Frontend assets.
add_action( 'enqueue_block_assets', 'embedpress_pro_blocks_cgb_block_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function embedpress_pro_blocks_cgb_editor_assets() { // phpcs:ignore
	// Scripts.

	// Register block editor script for backend.
	wp_enqueue_script(
		'embedpress_pro-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ), // Dependencies, defined above.
		null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
		true // Enqueue the script in the footer.
	);

	// Register block editor styles for backend.
	wp_enqueue_style(
		'embedpress_pro-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);

	// WP Localized globals. Use dynamic PHP stuff in JavaScript via `embedpressYoutubeObj` object.
	$youtube_options = YoutubePlugin::getOptions();
	$youtube_params = YoutubePlugin::getParams($youtube_options);
	$vimeo_options  = VimeoPlugin::getOptions();
	$vimeo_params = VimeoPlugin::getParams($vimeo_options);
	$elements = (array) get_option( EMBEDPRESS_PLG_NAME.":elements", []);
	$active_blocks = isset( $elements['gutenberg']) ? (array) $elements['gutenberg'] : [];
	wp_localize_script(
		'embedpress_pro-cgb-block-js',
		'embedpressProObj', // Array containing dynamic data for a JS Global.
		[
			'pluginDirPath' => plugin_dir_path( __DIR__ ),
			'pluginDirUrl'  => plugin_dir_url( __DIR__ ),
			'youtubeParams'	=> $youtube_params,
			'vimeoParams'	=> $vimeo_params,
			'active_blocks' => $active_blocks,
			// Add more data here that you want to access from `embedpressYoutubeObj` object.
		]
	);
}

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'embedpress_pro_blocks_cgb_editor_assets' );

add_filter('embedpress_document_block_powered_by',function (){
	return false;
});
